<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Post::class);
    }

    // /**
    //  * @return Post[] Returns an array of Post objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Post
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findByPost($id)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT count(*) as likes FROM posts p INNER JOIN user_like ul ON p.id = ul.post_id INNER JOIN users u on ul.user_id = u.id WHERE p.id = :id';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $id]);
        return $stmt->fetchAll();
    }

    public function findAllPosts()
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT posts.*,users.username,users.email FROM posts INNER JOIN users on posts.user_id = users.id ORDER by posts.id DESC';
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function findByLike($post_id)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT t1.*,t0.username,t0.email FROM users t0 INNER JOIN user_like t1 ON t0.id = t1.user_id WHERE t1.post_id = :id';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $post_id]);
        return $stmt->fetchAll();

    }

    public function findCommentByPost($post_id)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT comments.*,users.username,users.email,users.id as user_id FROM comments INNER JOIN posts ON comments.post_id = posts.id INNER Join users on comments.user_id = users.id WHERE post_id = :id';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $post_id]);
        return $stmt->fetchAll();
    }

    public function removeLike($post_id, $user_id)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'DELETE FROM user_like WHERE user_id= :user_id AND post_id = :post_id';
        $stmt = $conn->prepare($sql);
        return $stmt->execute(['post_id' => $post_id, 'user_id' => $user_id]);
    }
}
