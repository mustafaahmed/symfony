<?php

namespace App\Repository;

use App\Entity\Comment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    public function findCommentByPost($post_id, $order_by = '')
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT comments.*,users.username,users.email FROM comments INNER JOIN posts ON comments.post_id = posts.id INNER Join users on comments.user_id = users.id WHERE post_id = :id' . $order_by;
        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $post_id]);
        return $stmt->fetch();
    }

    public function findCommentByUser($comment_id, $user_id)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT * FROM comments WHERE user_id = :user_id AND id = :comment_id';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['user_id' => $user_id, 'comment_id' => $comment_id]);
        return $stmt->fetch();
    }

    public function removeById($comment_id)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'DELETE FROM comments WHERE id = :comment_id';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['comment_id' => $comment_id]);
        return $stmt->fetch();
    }
}
