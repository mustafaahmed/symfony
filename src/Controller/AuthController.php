<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends AbstractController
{
    /**
     * @Route("/register", name="register", methods={"GET"})
     */
    public function register()
    {
        return $this->render('auth/register.html.twig');
    }

    /**
     * @Route("/login", name="login.get", methods={"GET"})
     */
    public function login()
    {
        return $this->render('auth/login.html.twig');
    }

    /**
     * @Route("/register", name="register.post", methods={"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function store(Request $request)
    {
        $username = $request->get('username');
        $email = $request->get('email');
        $password = $request->get('password');
        $entityManager = $this->getDoctrine()->getManager();
      //  md5($password)
        $user = new User();
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setPassword(md5($password));
        $entityManager->persist($user);
        $entityManager->flush();
        return $this->redirect('/');
    }

    /**
     * @Route("/login", name="login.post", methods={"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginCheck(Request $request)
    {
        $username = $request->get('username');
        $password = $request->get('password');
        $user = $this->getDoctrine()->getRepository(User::class)->findByUsernamePassword($username, md5($password));

        if (!empty($user)) {
            $session = new Session();
            //   $session->start();
            $session->set('user', $user);
        }
        // return $this->json($user);
        return $this->redirect('/');
    }

    /**
     * @Route("/logout", name="logout.get", methods={"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function logout()
    {
        $session = new Session();
        $session->remove('user');
        return $this->redirect('/login');
    }
}
