<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class CommentController extends AbstractController
{
    /**
     * @Route("/comment", name="comment",methods={"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $comment_content = $request->get('comment');
        $post_id = $request->get('post_id');
        $session = new Session();
        $user_session = $session->get('user');
        if (empty($user_session)) {
            return $this->redirect('/login');
        }
        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository(User::class)->find($user_session['id']);
        $post = $this->getDoctrine()->getRepository(Post::class)->find($post_id);
        $comment = new Comment();
        $comment->setContent($comment_content);
        $comment->setUser($user);
        $comment->setPost($post);
        $entityManager->persist($comment);
        $entityManager->flush();
        $comment = $this->getDoctrine()->getRepository(Comment::class)->findCommentByPost($post_id, ' ORDER BY comments.id DESC');
        return $this->render('comment/comment.html.twig', compact('comment'));
    }

    /**
     * @Route("/comment/remove", name="comment.remove",methods={"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function remove(Request $request)
    {
        $id = $request->get('id');
        $session = new Session();
        $user_session = $session->get('user');
        if (empty($user_session)) {
            return $this->redirect('/login');
        }
        $comment = $this->getDoctrine()->getRepository(Comment::class)->findCommentByUser($id, $user_session['id']);
        if ($comment) {
            $this->getDoctrine()->getRepository(Comment::class)->removeById($id);
        }
        return $this->json(['status' => 'deleted']);
    }
}
