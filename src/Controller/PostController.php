<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{
    /**
     * @Route("/", name="posts", methods={"GET"})
     */
    public function index()
    {
        $session = new Session();
        //  $session->start();
        $user = $session->get('user');
        if (empty($user)) {
            return $this->redirect('/login');
        }

        $posts = $this->getDoctrine()->getRepository(Post::class)->findAllPosts();
        foreach ($posts as $key => $post) {
            $like = $this->getDoctrine()->getRepository(Post::class)->findByLike($post['id']);
            $comment = $this->getDoctrine()->getRepository(Post::class)->findCommentByPost($post['id']);
            $posts[$key]['comment'] = $comment;
            $posts[$key]['like'] = $like;
        }
        //  return $this->json( $posts);
        return $this->render('post/posts.html.twig', ['posts' => $posts]);
    }

    /**
     * @Route("/like", name="like",methods={"POST"})
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function like(Request $request)
    {
        $session = new Session();
        $user_session = $session->get('user');
        if (empty($user_session)) {
            return $this->redirect('/login');
        }
        $post_id = $request->get('post_id');
        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository(User::class)->find($user_session['id']);
        $post = $this->getDoctrine()->getRepository(Post::class)->find($post_id);
        $post->getLike()->add($user);
        $entityManager->persist($post);
        $entityManager->flush();
        return $this->json(['status' => 'liked']);
    }

    /**
     * @Route("/like/remove", name="like.remove",methods={"POST"})
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function remove_like(Request $request)
    {
        $session = new Session();
        $user_session = $session->get('user');
        if (empty($user_session)) {
            return $this->redirect('/login');
        }
        $post_id = $request->get('post_id');
        $this->getDoctrine()->getRepository(Post::class)->removeLike($post_id, $user_session['id']);
        return $this->json(['status' => 'disliked']);
    }

    /**
     * @Route("/post", name="post.store", methods={"POST"})
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function store(Request $request)
    {
        $session = new Session();
        $user_session = $session->get('user');
        if (empty($user_session)) {
            //return $this->redirect('/login');
            return $this->json('login');
        }
        $title = $request->get('title');
        $description = $request->get('description');
        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository(User::class)->find($user_session['id']);
        $post = new Post();
        $post->setTitle($title);
        $post->setDescription($description);
        $post->setUser($user);
        $entityManager->persist($post);
        $entityManager->flush();
        return $this->json('ok');
    }
}
