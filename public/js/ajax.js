$(function () {
    // Like
    $('.add_to_list[data-selected=false]').on('click', function (event) {
        event.preventDefault();
        var _this = $(this);
        var post_id = _this.attr("data-post");
        var like_count = parseInt(_this.children("#count-like").text());
        $.ajax({
            type: "POST",
            url: "/like",
            data: {post_id: post_id},
            success: function (response) {
                if (response.status === 'liked') {
                    _this.toggleClass('btn-default btn-info');
                    _this.children("#count-like").text(like_count + 1);
                    _this.attr('data-selected', true);
                    location.reload();
                }
            }
        });
    });

    // Dislike
    $('.add_to_list[data-selected=true]').on('click', function (event) {
        event.preventDefault();
        var _this = $(this);
        var post_id = _this.attr("data-post");
        var like_count = parseInt(_this.children("#count-like").text());
        _this.attr("data-selected", false);
        _this.parent().children("#count-like").text(like_count - 1);
        $.ajax({
            type: "POST",
            url: "/like/remove",
            data: {post_id: post_id},
            success: function (response) {
                if (response.status === 'disliked') {
                    _this.toggleClass('btn-info btn-default');
                    _this.children("#count-like").text(like_count - 1);
                    _this.attr('data-selected', false);
                    location.reload();
                }
            }
        });
    });
});

$(function () {
    $("#share").on("click", function (event) {
        event.preventDefault();
        var data = $("#share-form").serialize();
        $.ajax({
            type: "POST",
            url: "/post",
            data: data,
            success: function (response) {
                location.reload();
            },
            error: function (err) {
                //
            }
        });
    });

    $("#comment-submit").on("click", function (event) {
        event.preventDefault();
        var post_id = $(this).attr("data-comment-post");
        var comment = $("input[name=comment-content]").val();
        $.ajax({
            type: "POST",
            url: "/comment",
            data: {post_id: post_id, comment: comment},
            success: function (response) {
                if (response.status = 'success') {
                    $("ul.comments-list:last").after(response);
                    location.reload();
                }
            },
            error: function (err) {
                //
            }
        });
    });

    $("#remove-comment").on("click", function (event) {
        event.preventDefault();
        var comment_id = $(this).attr("data-comment-id");
        $.ajax({
            type: "POST",
            url: "/comment/remove",
            data: {id: comment_id},
            success: function (response) {
                if (response.status = 'deleted') {
                    $(this).parent().parent().remove();
                }
                location.reload();
            },
            error: function (err) {
                location.reload();
            }
        });
    });
});
